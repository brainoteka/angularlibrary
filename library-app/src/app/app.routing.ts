import { LibraryComponent } from './components/library/library.component';
import { AuthorizationComponent } from './components/auth/authorization.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
    { path: 'auth', component: AuthorizationComponent},
    { path: 'app', component: LibraryComponent},
    { path: '**', redirectTo: '/app'}
]