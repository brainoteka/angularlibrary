import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Article } from '../../models/article'
import { ArticleService } from '../../services/article.service'

@Component({
  selector: 'library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {
  articles: Article[] = [];
  checkedArticles: Article[] = [];

  constructor(private articleService: ArticleService) { 
    this.articleService.getArticles().then(articles => this.articles = articles);
    this.articleService.getArticleById(1).then(res => console.log(res)); 
  }

  addArticle(article: Article){
    this.articleService.addArticle(article).then(article => this.articles.unshift(article));
  }

  checkArticle(checked: boolean, article: Article){
    if(checked){
      this.checkedArticles.push(article);
    }else{
      this.checkedArticles = this.checkedArticles.filter(a => a !== article);
    }
  }

  delete(){
    const promises = this.checkedArticles.map(article => this.articleService.deleteArticle(article.id));
    Promise.all(promises).then(() => {
      this.articles = this.articles.filter( article => !this.checkedArticles.some(a => a === article));
      this.checkedArticles = [];
    });
  }

  ngOnInit() {
  }

}
