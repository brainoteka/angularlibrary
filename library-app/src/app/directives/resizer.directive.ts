import { Directive, ElementRef, HostBinding, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[resizer]',
  host: {
    '(mouseenter)' : 'mouseEnter()',
    '(mouseleave)' : 'mouseLeave()'
  }
})
export class ResizerDirective {
  @Input() kof: number = 1;

  constructor(private element: ElementRef) { }

  @HostBinding("style.width") get getWidth(){
    return `${this.kof * 100}px`;
  }

  @HostBinding("style.height") get getHeight(){
    return `${this.kof * 200}px`;
  }

  mouseEnter(){
    this.element.nativeElement.style.width = `${this.element.nativeElement.offsetWidth * this.kof}px`;
    this.element.nativeElement.style.height = `${this.element.nativeElement.offsetHeight * this.kof}px`
  }

  mouseLeave(){
    this.element.nativeElement.style.width = `${this.element.nativeElement.offsetWidth / this.kof}px`;
    this.element.nativeElement.style.height = `${this.element.nativeElement.offsetHeight / this.kof}px`
  }
}
