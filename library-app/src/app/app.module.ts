import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LibraryComponent } from './components/library/library.component';
import { ArticleComponent } from './components/article/article.component';
import { AddArticleComponent } from './components/add-article/add-article.component';
import { AuthorizationComponent } from './components/auth/authorization.component';

import {MdToolbarModule,
        MdCardModule,
        MdButtonModule,
        MdInputModule,
        MdCheckboxModule,
        MdIconModule,
        MdTabsModule} from '@angular/material';

import 'hammerjs';

import { ResizerDirective } from './directives/resizer.directive';
import { ArticleService } from './services/article.service'

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MemoryArticleService } from './api/article.api'

import { appRoutes } from './app.routing'

@NgModule({
  declarations: [
    AppComponent,
    LibraryComponent,
    ArticleComponent,
    AddArticleComponent,
    AuthorizationComponent,
    ResizerDirective
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    MdToolbarModule,
    MdCardModule,
    MdButtonModule,
    MdInputModule,
    MdCheckboxModule,
    MdIconModule,
    MdTabsModule,
    InMemoryWebApiModule.forRoot(MemoryArticleService)
  ],
  exports: [
    MdToolbarModule,
    MdCardModule,
    MdButtonModule,
    MdInputModule,
    MdCheckboxModule,
    MdIconModule,
    MdTabsModule
  ],
  providers: [ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
